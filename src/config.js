export const baseUrl = 'http://142.93.24.251/api/v1/'
export const businessUnitsUrl = baseUrl + 'users_business_units/'
export const authUrl = baseUrl + 'rest-auth/'

 // auth urls
 export const loginUrl = authUrl + 'login/'
 export const registerUrl = authUrl + 'registration/'
 export const logoutUrl = authUrl + 'logout/'
